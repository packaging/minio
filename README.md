# [Gitea](https://minio.io) Debian/Ubuntu Package Repo

Minio packages are already there (see [MinIO Object Storage for Linux](https://min.io/docs/minio/linux/index.html)), but not available es repo.

### Add repo signing key to apt

```
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-minio.asc https://packaging.gitlab.io/minio/gpg.key
```

### Add repo to apt

```
echo "deb https://packaging.gitlab.io/minio minio main" | sudo tee /etc/apt/sources.list.d/morph027-minio.list
```

### Install

```
sudo apt-get update
sudo apt-get install minio morph027-keyring
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
cat > /etc/apt/apt.conf.d/50minio <<EOF
Unattended-Upgrade::Allowed-Origins {
	"morph027:minio";
};
EOF
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
